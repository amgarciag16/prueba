﻿using Domain.Models;
using Domain.Mapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Actions
{
    public class EmployeesGet 
    {
        private IEmployeesGet Iemployeesget;
        private const string HOURLY_SALARY = "HourlySalaryEmployee";
        private const string MONTLY_SALARY = "MonthlySalaryEmployee";
        private const int VALOR = 120;

        public EmployeesGet(IEmployeesGet IEmployeesGet)
        {
            Iemployeesget = IEmployeesGet;
        }
        public async Task<Tuple<IEnumerable<Employee>, double, double>> GetEmployee()
        {
            IEnumerable<Employee> employ = await Iemployeesget.GetEmployees();
            Tuple<IEnumerable<Employee>, double, double> employee = new Tuple<IEnumerable<Employee>, double, double>(employ, 0,0);
            foreach (var item in employ)
            {
                if (item.ContractTypeName == HOURLY_SALARY)
                {
                    employee = new Tuple<IEnumerable<Employee>, double, double>(employ, (VALOR * item.HourlySalary * 12), (item.MonthlySalary * 12));
                }
                if (item.ContractTypeName == MONTLY_SALARY)
                { employee = new Tuple<IEnumerable<Employee>, double, double>(employ, (VALOR * item.HourlySalary * 12), (item.MonthlySalary * 12)); }
            }
            return employee;
        }
    }
}