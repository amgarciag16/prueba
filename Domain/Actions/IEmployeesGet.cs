﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Actions
{
    public interface IEmployeesGet
    {
        Task<IEnumerable<Employee>> GetEmployees();
       
    }
}
