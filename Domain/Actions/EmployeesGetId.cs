﻿using Domain.Mapper;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Actions
{
    public class EmployeesGetId
    {
        private IEmployeesGetId IemployeesgetId;
        private const string HOURLY_SALARY = "HourlySalaryEmployee";
        private const string MONTLY_SALARY = "MonthlySalaryEmployee";
        private const int VALOR = 120;

        public EmployeesGetId(IEmployeesGetId IIEmployeesGetId)
        {
            IemployeesgetId = IIEmployeesGetId;
        }
        public async Task<Tuple<Employee, double>> GetEmployeeById(int id)
        {
            Employee employ = await IemployeesgetId.GetEmployeeById(id);
            Tuple<Employee, double> employee=new Tuple<Employee, double>(employ, 0);
            
            if (employ.ContractTypeName == HOURLY_SALARY)
            {
                employee = new Tuple<Employee, double>(employ,(VALOR*employ.HourlySalary*12));
            }
            if (employ.ContractTypeName == MONTLY_SALARY)
            { employee = new Tuple<Employee, double>(employ, (employ.MonthlySalary * 12)); }

            return employee;
        }
    }
}
