﻿using Domain.Exceptions;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Factory
{
    public class EmployeesFactory
    {
        private const string FIRST_NAME_REQUIRED = "Primer nombre es requerido";
        private const string LAST_NAME_REQUIRED = "El apellido es requerido";
        private const string BIRTH_DATE_REQUIRED = "La fecha de nacimiento es requerido";
        private const string TYPE_CONTRACT_REQUIRED = "El tipo de contrato es requerido";
        private const string HOURLY_SALARY_REQUIRED = "El valor en horas es requerido";
        private const string MONTLY_SALARY_REQUIRED = "El valor mensual es requerido";

        public static Employee CreateEmploy(string name, string roleName, string typeContractName, double hourlySalary, double monthlySalary)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ExceptionExists(FIRST_NAME_REQUIRED);
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ExceptionExists(LAST_NAME_REQUIRED);
            }

            if (string.IsNullOrEmpty(typeContractName))
            {
                throw new ExceptionExists(TYPE_CONTRACT_REQUIRED);
            }

            if (hourlySalary == 0)
            {
                throw new ExceptionExists(HOURLY_SALARY_REQUIRED);
            }

            if (monthlySalary == 0)
            {
                throw new ExceptionExists(MONTLY_SALARY_REQUIRED);
            }

            return new Employee()
            {
                Name = name,
                RoleName = roleName,
                ContractTypeName = typeContractName,
                HourlySalary = hourlySalary,
                MonthlySalary = monthlySalary
            };
        }
    }
}
