﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Mapper
{
    public class EmployeeMapper
    {
        public static IEnumerable<Employee> MapEntity(IEnumerable<Employee> employees)
        {
            var employee = new List<Employee>();
            foreach (var item in employees)
            {
                employee.Add(MapEntity(item));
            }
            return employee;
        }
        public static Employee MapEntity(Employee employee)
        {
            var employees = new Employee() {
                Name = employee.Name,
                RoleName = employee.RoleName,
                ContractTypeName=employee.ContractTypeName,
                HourlySalary=employee.HourlySalary,
                MonthlySalary=employee.MonthlySalary
            };
            return employees;
        }
    }
}
