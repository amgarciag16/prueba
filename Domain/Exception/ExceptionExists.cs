﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Exceptions
{
    public class ExceptionExists : Exception
    {
        public ExceptionExists(string message) : base (message)
        {

        }
    }
}
