﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class Employee
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string RoleName { get; set; }
        public double HourlySalary { get; set; }
        public double MonthlySalary { get; set; }
        public string ContractTypeName { get; set; }
    }
}
