﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.DTO
{
    public class EmployDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string RoleName { get; set; }
        public double HourlySalary { get; set; }
        public double MonthlySalary { get; set; }
        public string TypeContractName { get; set; }
        public double CalculateValue { get; set; }
    }
}
