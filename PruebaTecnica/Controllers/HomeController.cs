﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Domain.Actions;
using Domain.Mapper;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PruebaTecnica.DTO;
using PruebaTecnica.Models;
using Repository.Repository;

namespace PruebaTecnica.Controllers
{
    public class HomeController : Controller
    {
        private IEmployeesGetId IemployeesGetId;
        private IEmployeesGet IemployeesGet;
        private EmployeesGetId employeesGetId;
        private EmployeesGet employeesGet;
        private EmployDTO employDto;
        private const string HOURLY_SALARY = "HourlySalaryEmployee";
        private const string MONTLY_SALARY = "MonthlySalaryEmployee";

        public HomeController(IEmployeesGetId IEmployeesGetId, IEmployeesGet IEmployeesGet, EmployeesGetId EmployeesGetId, EmployeesGet EmployeesGet)
        {
            IemployeesGetId = IEmployeesGetId;
            IemployeesGet = IEmployeesGet;
            employeesGetId = EmployeesGetId;
            employeesGet = EmployeesGet;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Employ()
        {
            return View();
        }
        

        public async Task<IActionResult> Search(string id)
        {
            EmployDTO employDto = new EmployDTO();
            List<EmployDTO> empDto = new List<EmployDTO>();
            //List<EmployDTO> empDto = new List<EmployDTO>();
            if (!string.IsNullOrEmpty(id))
            {
                var emp= await IemployeesGetId.GetEmployeeById(Convert.ToInt32(id));
                var emp1 = employeesGetId.GetEmployeeById(Convert.ToInt16(id));
                var salary = employeesGetId.GetEmployeeById(Convert.ToInt16(id)).Result.Item2;
                employDto.Name = emp.Name;
                employDto.RoleName = emp.RoleName;
                employDto.TypeContractName = emp.ContractTypeName;
                employDto.HourlySalary = emp.HourlySalary;
                employDto.MonthlySalary = emp.MonthlySalary;
                employDto.CalculateValue = salary;

                empDto.Add(employDto);
            }
            else
            {
                var emp = await IemployeesGet.GetEmployees();
                var empList = employeesGet.GetEmployee();
                var salary = employeesGet.GetEmployee().Result.Item2;
                foreach (var item in emp)
                {
                    employDto.Name = item.Name;
                    employDto.RoleName = item.RoleName;
                    employDto.TypeContractName = item.ContractTypeName;
                    employDto.HourlySalary = item.HourlySalary;
                    employDto.MonthlySalary = item.MonthlySalary;
                    employDto.CalculateValue = item.ContractTypeName == HOURLY_SALARY? salary: employeesGet.GetEmployee().Result.Item3;
                    

                    empDto.Add(employDto);
                    employDto = new EmployDTO();
                }
                
            }
            return View(empDto);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}