﻿using Domain.Actions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Config.Dependencies
{
    public class Container
    {
        public static void Register(IServiceCollection service, IConfiguration configuration)
        {
            #region actions
            service.AddScoped<IEmployeesGetId, EmployeesGetIdRepository>();
            service.AddScoped<IEmployeesGet, EmployeesGetRepository>();
            service.AddScoped<EmployeesGetId>();
            service.AddScoped<EmployeesGet>();
            #endregion
        }
    }
}
