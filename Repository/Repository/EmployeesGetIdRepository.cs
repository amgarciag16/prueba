﻿using Domain.Actions;
using Domain.Factory;
using Domain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repository
{
    public class EmployeesGetIdRepository : IEmployeesGetId
    {
        private HttpClient _httpClient;
        private Uri BaseEndpoint { get; set; }

        public async Task<Employee> GetEmployeeById(int id)
        {
            _httpClient = new HttpClient();
            List<Employee> employList = new List<Employee>();
            Employee employ = new Employee();
            
            var response = await _httpClient.GetAsync("http://masglobaltestapi.azurewebsites.net/api/Employees");
            response.EnsureSuccessStatusCode();
            response.Headers.Add("Accept", "application/json");
            //var data = await response.Content.ReadAsStringAsync();
            var data = await response.Content.ReadAsStringAsync();
            employList = JsonConvert.DeserializeObject<List<Employee>>(data);
            
            employ = employList.Find(x => x.ID == id);
            return EmployeesFactory.CreateEmploy(employ.Name, employ.RoleName, employ.ContractTypeName, employ.HourlySalary, employ.MonthlySalary);
        }
    }
}
