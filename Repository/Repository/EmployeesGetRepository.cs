﻿using Domain.Actions;
using Domain.Factory;
using Domain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repository
{
    public class EmployeesGetRepository : IEmployeesGet
    {
        private HttpClient _httpClient;
        private Uri BaseEndpoint { get; set; }
        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            _httpClient = new HttpClient();
            List<Employee> employList = new List<Employee>();
            List<Employee> employees = new List<Employee>();
            Employee employ = new Employee();

            var response = await _httpClient.GetAsync("http://masglobaltestapi.azurewebsites.net/api/Employees");
            response.EnsureSuccessStatusCode();
            response.Headers.Add("Accept", "application/json");

            var data = await response.Content.ReadAsStringAsync();
            employList = JsonConvert.DeserializeObject<List<Employee>>(data);

            foreach (var item in employList)
            {
                employees.Add(EmployeesFactory.CreateEmploy(item.Name, item.RoleName, item.ContractTypeName, item.HourlySalary, item.MonthlySalary));
            }

            return employees;
        }
    }
}
